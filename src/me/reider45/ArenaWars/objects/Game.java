package me.reider45.ArenaWars.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import me.reider45.ArenaWars.Main;
import me.reider45.ArenaWars.utilities.GameUtilities;

import org.bukkit.Location;

public class Game {

	State state;
	Integer maxPlayers;
	Integer lobbyTimer;
	Integer gameTimer;
	List<UUID> players = new ArrayList<UUID>();
	List<Location> spawns = new ArrayList<Location>();
	List<UUID> spectators = new ArrayList<UUID>();

	public Game()
	{
		// Create a new game
		this.state = State.LOBBY;
		this.maxPlayers = Main.getPlugin().getConfig().getInt("Game.MaxPlayers");
		this.lobbyTimer = Main.getPlugin().getConfig().getInt("Game.Timers.Lobby");
		this.gameTimer = Main.getPlugin().getConfig().getInt("Game.Timers.Game");
		
		for(String s : Main.getPlugin().getConfig().getStringList("Game.Spawns"))
		{
			this.spawns.add( GameUtilities.toLocation(s) );
		}
	}

	public State getState()
	{
		return this.state;
	}
	
	public void setState(State s)
	{
		this.state = s;
	}
	
	public Integer getMaxPlayers()
	{
		return this.maxPlayers;
	}
	
	public void setLobbyTimer(Integer i)
	{
		this.lobbyTimer = i;
	}

	public Integer getLobbyTimer()
	{
		return this.lobbyTimer;
	}
	
	public void setGameTimer(Integer i)
	{
		this.gameTimer = i;
	}

	public Integer getGameTimer()
	{
		return this.gameTimer;
	}

	public List<UUID> getPlayers()
	{
		return this.players;
	}
	
	public List<UUID> getSpectators()
	{
		return this.spectators;
	}

	public List<Location> getSpawns()
	{
		return this.spawns;
	}

}
