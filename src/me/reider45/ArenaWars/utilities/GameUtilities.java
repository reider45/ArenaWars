package me.reider45.ArenaWars.utilities;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class GameUtilities {
	
	public static Location 
	lobbySpawn,
	specSpawn;
	
	static String prefix(){
		return "�a�lArena Wars �7> ";
	}
	
	public static void broadcast(String msg)
	{
		Bukkit.broadcastMessage(prefix() + msg);
	}

	public static Location toLocation(String s)
	{
		String[] sp = s.split(",");
		return new Location(Bukkit.getWorld(sp[0]), Integer.parseInt(sp[1]),
				Integer.parseInt(sp[2]), Integer.parseInt(sp[3]));
	}

	public static String toString(Location l)
	{
		return l.getWorld().getName() + "," + l.getBlockX() + ","
				+ l.getBlockY() + "," + l.getBlockZ();
	}
	
}
