package me.reider45.ArenaWars;

import me.reider45.ArenaWars.events.PlayerDeath;
import me.reider45.ArenaWars.events.PlayerEvents;
import me.reider45.ArenaWars.events.PlayerJoin;
import me.reider45.ArenaWars.game.GameManager;
import me.reider45.ArenaWars.objects.Game;
import me.reider45.ArenaWars.timers.LobbyTimer;
import me.reider45.ArenaWars.utilities.GameUtilities;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	public static Main plugin;
	public static Game game;
	public static Main getPlugin()
	{
		return plugin;
	}
	
	public void onEnable()
	{
		plugin = this;
		saveDefaultConfig();
		
		if(getConfig().getInt("Game.MaxPlayers") > getConfig().getStringList("Game.Spawns").size())
		{
			System.out.println("[ERROR] There are more players than there are spawns set! Spawn points will be re-used");
		}
		
		// Register our locations
		GameUtilities.lobbySpawn = GameUtilities.toLocation(getConfig().getString("Game.Lobby"));
		GameUtilities.specSpawn = GameUtilities.toLocation(getConfig().getString("Game.Spectator"));
		
		// Create our game object
		game = new Game();
		
		// Start our lobby's timer
		LobbyTimer timer = new LobbyTimer();
		timer.startLobbyTimer();
		
		Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerDeath(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerEvents(), this);
	}
	
	public void onDisable()
	{
		GameManager manager = new GameManager();
		for(Player p : Bukkit.getOnlinePlayers())
		{
			if(game.getSpectators().contains(p.getUniqueId()))
			{
				manager.removeSpec(p);
			}
		}
		
		plugin = null;
	}

}
