package me.reider45.ArenaWars.game;

import java.util.UUID;

import me.reider45.ArenaWars.Main;
import me.reider45.ArenaWars.objects.Game;
import me.reider45.ArenaWars.objects.State;
import me.reider45.ArenaWars.timers.GameTimer;
import me.reider45.ArenaWars.utilities.GameUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GameStart {

	public void startGame()
	{
		Game game = Main.game;
		game.setState(State.INGAME);

		int i = 0;
		for(UUID id : game.getPlayers())
		{
			Player p = Bukkit.getPlayer(id);
			if(i < (game.getMaxPlayers()))
			{
				// Equip all players
				p.setHealth(20.0);
				p.setFoodLevel(20);
				p.getInventory().clear();
				p.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET, 1));
				p.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE, 1));
				p.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS, 1));
				p.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS, 1));
				p.getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD, 1));
				p.getInventory().addItem(new ItemStack(Material.APPLE, 3));

				// Teleport players
				if(game.getSpawns().size() > i)
				{
					p.teleport(game.getSpawns().get(i));	
				}else{
					i = 0;
					p.teleport(game.getSpawns().get(i));
				}
				i++;
			}else{
				GameManager manager = new GameManager();
				manager.makeSpec(p);
				p.sendMessage("�6You were added as a spectator since the player limit per game was reached!");
			}
		}

		GameUtilities.broadcast("�6The game has begun! Fight to the death!");

		GameTimer timer = new GameTimer();
		timer.startGameTimer();


	}

}
