package me.reider45.ArenaWars.game;

import me.reider45.ArenaWars.Main;
import me.reider45.ArenaWars.objects.Game;
import me.reider45.ArenaWars.objects.State;
import me.reider45.ArenaWars.timers.LobbyTimer;
import me.reider45.ArenaWars.utilities.GameUtilities;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class GameStop {

	public void stopGame()
	{
		Game game = Main.game;
		game.setState(State.RESTARTING);
		
		Player winner = Bukkit.getPlayer(game.getPlayers().get(0));
		
		if(winner != null)
		{
			GameUtilities.broadcast("�a"+winner.getName()+" �6has won the game!");
		}
		
		GameManager manager = new GameManager();
		for(Player p : Bukkit.getOnlinePlayers())
		{
			// Equip all players
			p.setHealth(20.0);
			p.setFoodLevel(20);
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			
			if(game.getSpectators().contains(p.getUniqueId()))
			{
				manager.removeSpec(p);
			}
			
			if(game.getPlayers().contains(p.getUniqueId()))
			{
				manager.removePlayer(p);
			}
		}
		
		// Reset the game
		Main.game = new Game();
		
		for(Player p : Bukkit.getOnlinePlayers())
		{
			// Add all players to the game
			manager.addPlayer(p);
		}
		
		// Start lobby timer
		LobbyTimer timer = new LobbyTimer();
		timer.startLobbyTimer();
		
	}
}
