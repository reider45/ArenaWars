package me.reider45.ArenaWars.game;

import me.reider45.ArenaWars.Main;
import me.reider45.ArenaWars.utilities.GameUtilities;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;


public class GameManager {
	
	public void addPlayer(Player p)
	{
		Main.game.getPlayers().add(p.getUniqueId());
		p.setGameMode(GameMode.SURVIVAL);
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		p.setHealth(20.0);
		p.setFoodLevel(20);
		p.teleport(GameUtilities.lobbySpawn);

		for (PotionEffect e : p.getActivePotionEffects())
		{
			p.removePotionEffect(e.getType());
		}
	}

	public void removePlayer(Player p)
	{
		Main.game.getPlayers().remove(p.getUniqueId());
	}

	public void makeSpec(Player p)
	{
		Main.game.getSpectators().add(p.getUniqueId());
		p.setGameMode(GameMode.SPECTATOR);
		p.teleport(GameUtilities.specSpawn);
	}

	public void removeSpec(Player p)
	{
		Main.game.getSpectators().remove(p.getUniqueId());
		p.setGameMode(GameMode.SURVIVAL);
	}
	
}
