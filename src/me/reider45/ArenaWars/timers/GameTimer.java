package me.reider45.ArenaWars.timers;

import me.reider45.ArenaWars.Main;
import me.reider45.ArenaWars.game.GameStop;
import me.reider45.ArenaWars.objects.Game;
import me.reider45.ArenaWars.objects.State;
import me.reider45.ArenaWars.utilities.GameUtilities;

import org.bukkit.Bukkit;

public class GameTimer {

	// Stores our timer so we can cancel it
	Integer ID;
	
	public void startGameTimer()
	{
		ID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
			public void run()
			{
				Game game = Main.game;
				int time = game.getGameTimer();
				
				if(game.getState() != State.INGAME)
				{
					// Verify that the game timer only runs during game
					cancelTimer();
				}
				
				if(time > 0)
				{
					game.setGameTimer(time-1);
					
					// Broadcast time
					if(time <= 10 || time == 60)
					{
						GameUtilities.broadcast("�6Game ending in "+time+" seconds!");
					}else
					if(time % 60 == 0)
					{
						GameUtilities.broadcast("�6Game ending in "+(time/60)+" minutes!");
					}
					
				}else
				if(time == 0)
				{
					// End the game here
					cancelTimer();
					GameStop gs = new GameStop();
					gs.stopGame();
					
				}
			}
		}, 20L, 20L);
		
	}
	
	void cancelTimer()
	{
		Bukkit.getScheduler().cancelTask(ID);
	}

}
