package me.reider45.ArenaWars.timers;

import me.reider45.ArenaWars.Main;
import me.reider45.ArenaWars.game.GameStart;
import me.reider45.ArenaWars.objects.Game;
import me.reider45.ArenaWars.objects.State;
import me.reider45.ArenaWars.utilities.GameUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class LobbyTimer {

	// Stores our timer so we can cancel it
	Integer ID;
	
	public void startLobbyTimer()
	{
		ID = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
			public void run()
			{
				Game game = Main.game;
				int time = game.getLobbyTimer();
				
				if(game.getState() != State.LOBBY)
				{
					// Verify that the lobby timer only runs during lobby
					cancelTimer();
				}
				
				if(time > 0)
				{
					game.setLobbyTimer(time-1);
					
					// Set player levels to time
					for(Player p : Bukkit.getOnlinePlayers()){ p.setLevel(time); }
					
					// Broadcast time
					if(time <= 10 || time == 60)
					{
						GameUtilities.broadcast("�aThe game is starting in "+time+" seconds!");
						for(Player p : Bukkit.getOnlinePlayers())
						{
							p.playSound(p.getLocation(), Sound.NOTE_PIANO, 1.0f, 1.0f);
						}
					}
					
				}else
				if(time == 0)
				{
					// Check to start the game
					if(game.getPlayers().size() >= Main.getPlugin().getConfig().getInt("Game.MinPlayers"))
					{
						System.out.println(game.getPlayers().size());
						// Start the game
						cancelTimer();
						GameStart gs = new GameStart();
						gs.startGame();
						
					}else{
						// Not enough players to begin
						game.setLobbyTimer(Main.getPlugin().getConfig().getInt("Game.Timers.Lobby"));
						GameUtilities.broadcast("�cNot enough players to begin the match!");
					}
					
				}
			}
		}, 20L, 20L);
		
	}
	
	void cancelTimer()
	{
		Bukkit.getScheduler().cancelTask(ID);
	}
}
