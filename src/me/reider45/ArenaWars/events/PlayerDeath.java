package me.reider45.ArenaWars.events;

import me.reider45.ArenaWars.Main;
import me.reider45.ArenaWars.game.GameManager;
import me.reider45.ArenaWars.game.GameStop;
import me.reider45.ArenaWars.objects.Game;
import me.reider45.ArenaWars.objects.State;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeath implements Listener {
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e)
	{
		Player p = (Player) e.getEntity();
		Game game = Main.game;
		
		if(game.getState() == State.INGAME && game.getPlayers().contains(p.getUniqueId()))
		{
			GameManager manager = new GameManager();
			manager.removePlayer(p);
			manager.makeSpec(p);
			
			if(game.getPlayers().size() == 1)
			{
				// End game if there's only one player
				GameStop gs = new GameStop();
				gs.stopGame();
			}
		}
		e.getDrops().clear();
	}
	

	

}
