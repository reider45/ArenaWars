package me.reider45.ArenaWars.events;

import me.reider45.ArenaWars.Main;

import me.reider45.ArenaWars.game.GameManager;
import me.reider45.ArenaWars.objects.Game;
import me.reider45.ArenaWars.objects.State;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerJoin implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e)
	{
		Player p = e.getPlayer();
		Game game = Main.game;
		
		GameManager manager = new GameManager();
		if(game.getState() == State.LOBBY)
		{	
			if(game.getPlayers().size() < game.getMaxPlayers() )
			{
				// Add player to the lobby
				manager.addPlayer(p);
			}
		}else
		if(game.getState() == State.INGAME)
		{
			e.setJoinMessage(null);
			manager.makeSpec(p);
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e)
	{
		Player p = e.getPlayer();
		Game game = Main.game;
		
		if(game.getPlayers().contains(p.getUniqueId()))
		{
			GameManager manager = new GameManager();
			manager.removePlayer(p);
		}
		
	}

}
